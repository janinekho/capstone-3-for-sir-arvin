import React, {useEffect, useState} from 'react';
import {Doughnut} from 'react-chartjs-2';
import {Bar} from 'react-chartjs-2';
import { Col, Form, Card} from 'react-bootstrap';
import DoughnutChart from '../../components/DoughnutChart'
import LineChart from '../../components/LineChart'
import moment from 'moment'
export default function dashboard({data,data1,data2}){
  /*const[income, setIncome]= useState([]);
  const[iamount, setiAmount] =useState(0);
  const [expense, setExpense] = useState([]);*/

  const[description, setDescription]= useState('');
  const[eDescription, seteDescription] = useState('');
  const[incomeLabel, setIncomeLabel] = useState([]);
  const[expenseLabel, setExpenseLabel] = useState([]);
  const [iamount, setiAmount] = useState([]);
  const[eamount,seteAmount]= useState([]);
  const[income, setIncome] = useState('');
  const [expense, setExpense] = useState('');
  const [bgColors, setBgColors] = useState([]);
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [dataResult, setDataResult] = useState([]);
  const [date, SetDate] = useState('');
   const [lineChartData, setLineChartData] = useState([]);
  const [lineChartLabel, setLineChartLabel] = useState([]);
  const[total, setTotal] = useState(0);
 	useEffect(()=>{
 		 displayData();
 		
    },[])



function preventDefault(e){
	e.preventDefault()
  displayData();
 }


 function displayData(){
   
 
 fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/transaction-details`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
        
        })
      .then(res => res.json())
      .then(data => {
        
      	
      	 let tempData = data;
    console.log(startDate)
    console.log(endDate)
if(startDate !='' && endDate !=''){
       var tempStartDate = new Date(startDate);
        var tempEndDate = new Date(endDate);
         var filterData = data.filter(a => {
          var date = new Date(a.date);
          return (date >= tempStartDate && date <= tempEndDate);
        });
        console.log(filterData)
        tempData = filterData;
}

       let totalMap = new Map();
        // remove duplicate category by summing them up
        for (var i =0; i< tempData.length; i++){
          let key = tempData[i].transactionDescription
           if(totalMap.has(key)){
             let entry = totalMap.get(key);
             entry.amount = entry.amount + tempData[i].amount;
             totalMap.set(key,entry);

           } else {
            totalMap.set(key,tempData[i]);
         }
        }
       

        // set values to label
        let tempIncome = [];
        let tempIncomeLabel = [];
        let tempExpense = [];
        let tempExpenseLabel = [];
        let total=0;

        for (let value of totalMap.values()) {
          if(value.transactionType == "Income"){
              tempIncome.push(value.amount);
              tempIncomeLabel.push(value.transactionDescription) 
              /*tempIncome.map( function(elt){ // assure the value can be converted into an integer
              return /^\d+$/.test(elt) ? parseInt(elt) : 0; 
              })
              .reduce( function(a,b){ // sum all resulting numbers
              return a+b
              }))*/

             

              

          } else if(value.transactionType == "expense"|| value.transactionType == "Expense"){
            tempExpense.push(value.amount)
            tempExpenseLabel.push(value.transactionDescription)
          }
          
        }

        
        setIncome(tempIncome);
        setIncomeLabel(tempIncomeLabel);
        setExpense(tempExpense);
        setExpenseLabel(tempExpenseLabel);
        
        setiAmount(parseInt(tempIncome))

        const sortedData = tempData.sort((a, b) => new Date(a.date) - new Date(b.date));
        console.log(sortedData);

        let tempLineData = [];
        let tempLineLabel = [];
        let curr = 0;
        for(var i = 0; i < sortedData.length; i++){
          tempLineLabel.push(new Date(sortedData[i].date).toLocaleString());
          curr = curr + sortedData[i].amount;
          var balance = {
            t: new Date(sortedData[i].date),
            y: curr
          };
          tempLineData.push(balance);

        }
        setLineChartData(tempLineData);
        setLineChartLabel(tempLineLabel);
        console.log(tempLineData)
        console.log(tempLineLabel)
       })
        

 }
 	return(
 		<React.Fragment>
        <Form>
          <Form.Row>
            <Col>
              <Form.Control type="Date"placeholder="Start Date" value={startDate} onChange= {e => {setStartDate(e.target.value)}}/>
            </Col>
             <Col>
              <Form.Control type="Date" placeholder="End Date" value={endDate} onChange={e => {setEndDate(e.target.value)}} />
            </Col>
             <Col>
                <button onClick={preventDefault}> Go  </button>
             </Col>
          </Form.Row>
        </Form>
 		 <h1> Reports </h1>
      <div className="Doughnut">
     <h5> Income </h5>
        
        <DoughnutChart  height="200px" width="200px"
        amount={income}
        label={incomeLabel}

        />

      <h5> Expense </h5>
        <DoughnutChart height="200px" width="200px"
        amount={expense}
        label={expenseLabel}
        />
       </div>

        <div className="linechart">
        <Card>
           <Card.Title>
             <h5 className="h3 mb-0">Line chart</h5>
           </Card.Title>
           
         
        <LineChart
         balanceData={lineChartData}
        label={lineChartLabel}
        />
       
        
        </Card>
        </div>
    </React.Fragment>
   
	 ) 
	
}

