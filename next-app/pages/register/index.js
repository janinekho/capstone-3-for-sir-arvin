import React, {useState, useEffect} from 'react';
import { Form, Button } from 'react-bootstrap';
import Head from 'next/head';
export default function index(){
  //all the first 3 strings will contain kung ano nasa fomr
  const[email, setEmail] = useState('');
  const[firstName, setfirstName] = useState('');
  const[lastName, setlastName] = useState('');
  const[mobileNo, setmobileNo] = useState('');
  const[password1, setPassword1] = useState('');
  const[password2, setPassword2] = useState('');
  //state to determine whether submit button is enabled or not
  const[isActive, setIsActive] = useState(false);

  console.log(email);
  console.log(password1);
 


    useEffect(() =>{
      
    },[])
   function registerUser(e) {
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            
        },
         body: JSON.stringify({
           firstName: firstName,
           lastName: lastName,
            email: email,
          mobileNo: mobileNo,
          password: password1,
          
        })

      },[])
        
      .then(res => res.json())
      .then(data => {
        console.log(data)
      })

    e.preventDefault();

    // clear input fields
    setEmail('');
    setfirstName('');
    setlastName('');
    setmobileNo('');
    setPassword1('');
    setPassword2('');

    alert('Thank you for registering!');
   }

   useEffect(() => {

    //validation to enable submit button when all fields are populated and both passwords match /*&&(password1 === password2))*/
    if((email !== '' && password1 !== '' && password2 !== '')) {
        setIsActive(true);
    }else {
        setIsActive(false);
    }
    
   }, [ email, password1, password2 ])

    return(
        
       
        <div className="register">
        <React.Fragment>
        
         <Head>
         <title className="registerText"> Register </title>
         </Head>
            <Form onSubmit={e => registerUser(e)}>

             <div>
             <h1 controlId="register-form" > Register Here </h1>
            <Form.Group>
                    <Form.Label> First Name </Form.Label>
                    <Form.Control 
                        type ="text"
                        value= {firstName}
                        onChange={e => setfirstName(e.target.value)}
                        required 
                />
                </Form.Group>
              <Form.Group>
                    <Form.Label> Last Name </Form.Label>
                    <Form.Control 
                        type ="text"
                        value= {lastName}
                        onChange={e => setlastName(e.target.value)}
                        required 
                />
                </Form.Group>

                <Form.Group controlId="mobileNo">
                    <Form.Label> Mobile No. </Form.Label>
                    <Form.Control 
                        type ="number"
                        value= {mobileNo}
                        onChange={e => setmobileNo(e.target.value)}
                        required 
                />
                </Form.Group>


                <Form.Group controlId="userEmail">
                    <Form.Label> Email Address </Form.Label>
                    <Form.Control 
                        type ="email"
                        placeholder = "Enter email"
                        value= {email}
                        onChange={e => setEmail(e.target.value)}
                        required 
                />
                    <Form.Text className = "text-muted">
                        We'll never share your email with anyone else.
                     </Form.Text>   
                </Form.Group>

                

                <Form.Group controlId="password1">
                    <Form.Label> Password </Form.Label>
                    <Form.Control 
                        type ="password"
                        placeholder = "Enter Password"
                        value = {password1}
                        onChange = {e => setPassword1(e.target.value)}
                        required 
                />  
                </Form.Group>

                <Form.Group controlId="password2">
                    <Form.Label> Verify Password </Form.Label>
                    <Form.Control 
                        type ="password"
                        placeholder = "Verify Password"
                        value = {password2}
                        onChange = {e => setPassword2(e.target.value)}
                        required 
                />                  
                </Form.Group>
                {/*conditionally render submit button based on inActive state*/}
                {isActive
                    ?
                    <Button className="bg-primary" type="submit" id="submitBtn" onSubmit={e => registerUser(e)}> Submit
                   </Button>  
                    :
                  <Button className="bg-danger" type="submit" id="submitBtn" disabled> Submit
                  </Button> 

                }
                    
                </div>
                </Form>
        </React.Fragment>
        </div>

        )
}