const express = require('express')
const router = express.Router();

const UserController = require('../controllers/user')
const auth = require('../auth')


//Check if email exists
router.post('/email-exists', (req, res) => {
    UserController.emailExists(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))
})

//User Registration
router.post('/', (req, res) => {
    UserController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))
})



router.post('/post-transaction', auth.verify, (req,res) =>{
    
   const params = {
        userId: auth.decode(req.headers.authorization).id,
        transactionDescription: req.body.transactionDescription,
        transactionType: req.body.transactionType,
        amount: req.body.amount,
        remarks: req.body.remarks
    }

    UserController.postTransaction(params).then(result => res.send(result))
})

router.post('/add-Category', (req,res) =>{
    
   const params = {
        
        type: req.body.type,
        description: req.body.description
        
    }

    UserController.addCategory(params).then(result => res.send(result))
})


//Login
router.post('/login', (req, res) => {
    UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

//Retrieve User Details
router.get('/details', auth.verify, (req, res) => {
    // user = {
    //  id: user._id,
    //  email: user.email,
    //  isAdmin: user.isAdmin
    // }
    const user = auth.decode(req.headers.authorization)
    UserController.get({ userId: user.id }).then(resultFromDetails => res.send(resultFromDetails))
})




//Retrieve Transaction Details
router.get('/transaction-details', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization)
    UserController.viewTransaction({ userId: user.id })
    .then(transactions => res.send(transactions))
})

/*//Transaction By Type
router.get('/:type', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization)
    UserController.transactionType({ transactionType: transactionType })
    .then(transactions => res.send(transactions))
})*/




router.get('/category', (req, res) => {
    
    UserController.getCategory().then(category => res.send(category))
})


router.put('/edit-user', auth.verify, (req, res) => {
    //where do we get the info that will get updated
    const params = {
        userId:auth.decode(req.headers.authorization).id,
        firstName: req.body.firstName,
        lastName: req.body.lastName
             
    }
    UserController.editProfile(params).then(result => res.send(result))
})


router.put('/:categoryId', auth.verify, (req, res) => {
    
    const params = {
        categoryId: req.params.categoryId,
        type: req.body.type,
        description: req.body.description
        
    }
    UserController.editCategory(params).then(result => res.send(result))
})

router.patch('/', auth.verify, (req, res) => {
    
    UserController.editTransaction(req.body).then(result => res.send(result))
})


//Verify Google Token
router.post('/verify-google-id-token', async(req,res)=>{
    res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})


router.delete('/:transactionId', auth.verify, (req, res) => {
    const transactionId = req.params.transactionId
    UserController.deleteTransaction(transactionId).then(result => res.send(result))
})
//Another way

/*router.post('/verify-google-id-token', (req,res)=>{
    UserController.verifyGoogleTokenId(req.body.tokenId)).then(res => res.send(result))
})*/


/*async function hello(){
    await hi()
}*/
/*
function hi(){
    console.log('hi')
}*/
module.exports = router