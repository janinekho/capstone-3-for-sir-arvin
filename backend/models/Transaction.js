const mongoose = require('mongoose')

const transactionSchema = new mongoose.Schema({
    transactionType: {
        type:String 
    },
    transactionDescription: {
        type:String
    },

    date:{
        type: Date,
        default: new Date()
    },
    amount: {
        type:Number
    },
    remarks: {
        type: String
    },
    userId:{
        type: String,
        required: [true, 'User ID is required.']
    },
    categoryId:{
        type: String,
        
    }

});

/*const categorySchema = new mongoose.Schema({
    transactionType: {
        type:String 
    },
    transactionDescription: {
        type:String
    }
});*/

module.exports = mongoose.model('transaction', transactionSchema)