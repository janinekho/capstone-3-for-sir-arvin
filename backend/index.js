const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')

const userRoutes = require('./routes/user')
/*const courseRoutes = require('./routes/course')*/

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://admin:admin123@cluster0.rxhch.mongodb.net/expense_tracker_app?retryWrites=true&w=majority', {
    
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use(cors());
app.options("*", cors());


/*const corsOptions = {
	origin: 'https://budget-tracker-final-capstone-3.vercel.app/', // Where our Next.js front-end would be served from
	optionsSuccessStatus: 200 // For compatibility with older browsers
}
*/
/*app.use(cors(corsOptions));*/


app.use('/api/users', userRoutes)
/*app.use('/api/courses', courseRoutes)*/

app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
})